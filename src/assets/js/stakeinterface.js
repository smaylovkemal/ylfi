const stakesc = "0xF0ca8e0B6Cc679D0D946b7470e26920601Ff1C37";
const yfdottoken = "0x186Af393bF9cEef31CE7EaE2b468C46231163cC7";
// const stakesc = "0xDC217613a926A10B1fdd20B9F03F95A575d5ed81";
// const yfdottoken = "0x2e6539edc3b76f1E21B71d214527FAbA875F70F3";
const etherscanlinktx = "https://www.etherscan.io/tx/";
const stakeabi = [{"inputs":[{"internalType":"address","name":"YLFi","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"staker","type":"address"},{"indexed":true,"internalType":"address","name":"tokenStakeTarget","type":"address"},{"indexed":true,"internalType":"uint256","name":"amountReward","type":"uint256"}],"name":"Claim","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"staker","type":"address"},{"indexed":true,"internalType":"address","name":"tokenStakeTarget","type":"address"},{"indexed":true,"internalType":"uint256","name":"amountTokenStaked","type":"uint256"}],"name":"Stake","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"staker","type":"address"},{"indexed":true,"internalType":"address","name":"tokenStakeTarget","type":"address"},{"indexed":true,"internalType":"uint256","name":"amountTokenStaked","type":"uint256"}],"name":"Unstake","type":"event"},{"inputs":[{"internalType":"uint256","name":"timePeriodStake","type":"uint256"},{"internalType":"uint256","name":"timeCooldownUnstake","type":"uint256"},{"internalType":"uint256","name":"formula1","type":"uint256"},{"internalType":"uint256","name":"formula2","type":"uint256"},{"internalType":"uint256","name":"fpel1","type":"uint256"},{"internalType":"uint256","name":"fpel2","type":"uint256"}],"name":"addPeriod","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"erc20Token","type":"address"},{"internalType":"uint256","name":"amountEqual","type":"uint256"},{"internalType":"string","name":"symboltokens","type":"string"}],"name":"addTokenReward","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"claimReward","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"periodEdit","type":"uint256"},{"internalType":"uint256","name":"timePeriodStake","type":"uint256"},{"internalType":"uint256","name":"timeCooldownUnstake","type":"uint256"},{"internalType":"uint256","name":"formula1","type":"uint256"},{"internalType":"uint256","name":"formula2","type":"uint256"},{"internalType":"uint256","name":"fpel1","type":"uint256"},{"internalType":"uint256","name":"fpel2","type":"uint256"}],"name":"editPeriod","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"erc20Token","type":"address"},{"internalType":"uint256","name":"amountEqual","type":"uint256"},{"internalType":"string","name":"symboltokens","type":"string"}],"name":"editTokenReward","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"getActiveStaker","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"erc20Token","type":"address"}],"name":"getEqualReward","outputs":[{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"periodwant","type":"uint256"}],"name":"getPeriodDetail","outputs":[{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getPeriodList","outputs":[{"internalType":"uint256[]","name":"","type":"uint256[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"tokenWantStake","type":"address"},{"internalType":"uint256","name":"amountWantStake","type":"uint256"},{"internalType":"uint256","name":"periodwant","type":"uint256"}],"name":"getRewardCalculator","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"stakerAddress","type":"address"}],"name":"getRewardClaimable","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"stakerAddress","type":"address"}],"name":"getRewardEstimator","outputs":[{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"stakerAddress","type":"address"}],"name":"getRewardObtained","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getTokenList","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getTotalStaker","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"stakerAddress","type":"address"}],"name":"getUserInfo","outputs":[{"internalType":"bool","name":"","type":"bool"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"address","name":"","type":"address"},{"internalType":"string","name":"","type":"string"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"tokenTargetStake","type":"address"},{"internalType":"uint256","name":"amountWantStake","type":"uint256"},{"internalType":"uint256","name":"periodwant","type":"uint256"}],"name":"stakeNow","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"unstakeNow","outputs":[],"stateMutability":"nonpayable","type":"function"}];

var scabi = [];
var listsc = [];
var listtoken = [];
var listsymbol = [];
var real = [];
var view = [];
var periodArray = [];
var menuperiod = '';
var periodestimatorhere = '';
var totalyfdotonstake;
var endstake;
var cooldown;
var claimdate;
var wantstaken;
var wantperiod;
var stakedsymbol;
var controllerstake;
var controllerstakemm;
var controllertokenmm;
var account = "0x0000000000000000000000000000000000000000";
var network;
var initmenu = false;
var inittx = true;

async function initInfo(){
    if(initmenu == false){
        initmenu = true;
        controllerstake = new window.web3.eth.Contract(stakeabi,stakesc);
        var tokenlisted = await controllerstake.methods.getTokenList().call();
        var periodList = await controllerstake.methods.getPeriodList().call();
        for(var y = 0; y < periodList.length; y++){
            var perSort = y+1;
            var periodDetail = await controllerstake.methods.getPeriodDetail(perSort).call();
            var percertPenalty = periodDetail[4] / periodDetail[5];
            var rppPercent = Math.floor((periodDetail[2] / periodDetail[3]) * periodDetail[0]);
            var unixPeriodToDay = Math.floor(periodDetail[0] / 86400);
            periodArray[perSort] = {"period":perSort,"periodDay":unixPeriodToDay,"rppPercent":rppPercent,"penaltyPercent":percertPenalty};
            menuperiod = menuperiod + '<a onclick="setPeriod('+perSort+')" class="dropdown-item">'+periodArray[perSort]['periodDay']+' Days</a>';
            periodestimatorhere = periodestimatorhere + '<div class="col-6 changepaddingcol"><p id="period'+perSort+'day" class="inlineinfo1 text-left">Period '+perSort+' : '+periodArray[perSort]['periodDay']+' Days</p></div>';
            periodestimatorhere = periodestimatorhere + '<div class="col-6 changepaddingcol"><p id="period'+perSort+'est" class="inlineinfo2 text-center">0 N/A</p></div>';
        }
        $("#estimatorperiodhere").html(periodestimatorhere);
        $("#perioddd").html(menuperiod);
        for(var x = 0; x < tokenlisted.length; x++){
            var tokenne = tokenlisted[x];
            listtoken[x] = tokenlisted[x];
            var jsonggg = $.getJSON("assets/abi/"+tokenne+".json", function(result){
                scabi[tokenne] = result;
                setTimeout(async function(){
                    listsc[tokenne] = new window.web3.eth.Contract(scabi[tokenne],tokenne);
                },500);
            });
        }
        setTimeout(function() {
            getTotalYfdotNow();
        }, 2000);
    }
}

async function getTotalYfdotNow() {
    var result = await listsc[yfdottoken].methods.balanceOf(stakesc).call();
    var getFrom18 = result / (10**18);
    $.getJSON("https://api.coingecko.com/api/v3/simple/price?ids=yearn-finance-dot&vs_currencies=usd", function(datas){
        var dataperyfdot = datas['yearn-finance-dot']['usd'];
        var howmuchusdlock = getFrom18 * dataperyfdot;
        $("#howmuchstaked").html(' <span class="lbluetext">Locked</span> $'+howmuchusdlock.toFixed(2));
    });
}

async function connectDapps(){
    // Modern dapp browsers...
    if (window.ethereum) {
        ethereum.autoRefreshOnNetworkChange = false;
        window.web3 = new Web3(ethereum);
        try {
            await ethereum.enable();
            // await ethereum.send('eth_requestAccounts');
            // await ethereum.request({
            //     method: 'eth_requestAccounts'
            // });
            initInfo();
            setTimeout(function(){
                getWallet();
                getNetworkType();
            }, 700);
        } catch (error) {
            connectDapps();
        }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
        window.web3 = new Web3(web3.currentProvider);
        initInfo();
        setTimeout(function(){
            getWallet();
            getNetworkType();
        }, 700);
    }
    // Non-dapp browsers...
    else {
        $('#notsupport').modal({backdrop: 'static',keyboard: true, show: true});
    }
}

async function setupSc(){
    controllertokenmm = new window.web3.eth.Contract(scabi[yfdottoken],yfdottoken);
    controllerstakemm = new window.web3.eth.Contract(stakeabi,stakesc);
}

async function getWallet(){
    account = await window.web3.eth.getAccounts();
    account = account[0];
    var viewaccount = account.substr(0, 5) + "....." + account.substr(39, 42);
    $("#unlockwalletnow").attr("disabled",true);
    $("#unlockwalletnow").html("<img src="+hqx(blockies.create({ seed:account ,size: 8,scale: 1}),4).toDataURL()+" style='width: 16px; height: auto; border-radius: 8px;' /> " + viewaccount);
}

async function getNetworkType(){
    network = await window.web3.eth.net.getId();
    if(network != 1){
        $('#network').modal({backdrop: 'static',keyboard: true, show: true});
    }else{
        setTimeout(function(){
            setupSc();
        }, 2000);
        setTimeout(function() {
            loader();
        }, 500);
    }
}

function loader(){
    setTimeout(function(){
        getYfdotBalance(account);
        setTimeout(function() {
            getStakerInfo(account);
        },300);
        setTimeout(function() {
            getYfdotBalance(account);
            getYfdotApproved(account);
            getStakeEstInfo(account);
            getObtained(account);
            getClaimable(account);
            getTotalYfdotNow();
        }, 600);
    }, 2100);
}

async function getYfdotBalance(wallet) {
    var checkbalance = await listsc[yfdottoken].methods.balanceOf(wallet).call();
    real["balanceyfdot"] = checkbalance;
    view["balanceyfdot"] = (checkbalance / (10**18)).toPrecision(8);
    $("#yfdotbalance").html("Available Balance : "+view["balanceyfdot"]+" YLFi");
}

async function getYfdotApproved(wallet) {
    var checkbalance = await listsc[yfdottoken].methods.allowance(wallet, stakesc).call();
    real["approvedyfdot"] = checkbalance;
    view["approvedyfdot"] = (checkbalance / (10**18)).toPrecision(8);
}

async function getClaimable(wallet) {
    var result = await controllerstake.methods.getRewardClaimable(wallet).call();
    real["claimablereward"] = result;
    view["claimablereward"] = (result / (10**18));
    $("#claimable").html(convert(view["claimablereward"]) + " " + stakedsymbol);
}

async function getObtained(wallet) {
    var result = await controllerstake.methods.getRewardObtained(wallet).call();
    real["obtainedreward"] = result;
    view["obtainedreward"] = (result / (10**18));
    $("#rewardstake").html(convert(view["obtainedreward"]) + " " + stakedsymbol);
}

async function getRewardCalculator(token, amount, afwa) {
    var to18 = amount * (10**18);
    var newbn = new BigNumber(to18);
    var result = await controllerstake.methods.getRewardCalculator(token.toString(), newbn, afwa).call();
    return result;
}

async function getStakerInfo(wallet) {
    var result = await controllerstake.methods.getUserInfo(wallet).call();
    if(result[0] == true){
        $(".blockchainvalidation").hide();
        $(".blockchainvalidationdone").show();
        $(".stake").hide();
        $(".unstake").show();
        $("#rpp").html(periodArray[result[1]]['rppPercent']/result[1]+"%");
        $("#periodview").html(periodArray[result[1]]['periodDay']+" Days");
        $("#penaltypc").html(periodArray[result[1]]['penaltyPercent']+"%");
        $("#rppview").html('<span class="lbluetext">RPP</span> '+periodArray[result[1]]['rppPercent']+'%');
        setTimeout(function() {
            var newunixtime = endstake * 1000;
            var datatiame = moment(newunixtime).format('MM/DD/YYYY HH:mm:ss');
            
            $('#timer').countdown(datatiame, {elapse: true}).on('update.countdown', function(event) {
                var $this = $(this);
                if (event.elapsed) {
                    $this.html('<center> <table class="table table-borderless"> <thead> <tr> <th id="exceptth"><h4>You might unstake your balance without penalty now.</h4></th> </tr></thead> </table> </center>');
                } else {
                    $this.html('<center> <table class="table table-borderless"> <thead> <tr> <th><h1>' + event.strftime('%D') + '</h1></th> <th><h1>:</h1></th><th><h1>' + event.strftime('%H') + '</h1></th> <th><h1>:</h1></th><th><h1>' + event.strftime('%M') + '</h1></th> <th><h1>:</th><th><h1>' + event.strftime('%S') + '</h1></th> </tr></thead> <tbody> <tr> <td>Day</td> <th></th><td>Hour</td> <th></th><td>Minute</td> <th></th><td>Second</td> </tr></tbody> </table> </center>');
                }
            });
        }, 100);   
    }else{
        if(inittx == true){
            inittx = false;
            $("#rppview").html('<span class="lbluetext">RPP</span> 0%');
            $("#perioddropdown").html("Select Period");
            wantperiod = null;
        }
        $(".blockchainvalidation").hide();
        $(".blockchainvalidationdone").show();
        $(".stake").show();
        $(".unstake").hide();
    }
    stakedsymbol = result[3];
    claimdate = result[6];
    endstake = result[7];
    cooldown = result[8];
    real["stakedbalance"] = result[4];
    real["totalreward"] = result[9];
    view["stakedbalance"] = (result[4] / (10**18));
    view["totalreward"] = (result[9] / (10**18));
    var totalbal = parseFloat(view["balanceyfdot"]) + parseFloat(view["stakedbalance"]);
    $("#totalreward").html(convert(view["totalreward"]) + " " + stakedsymbol);
    $("#initbalance").html(convert(view["stakedbalance"]) + " YLFi");
    $("#freebalance").html(convert(totalbal) + " YLFi");
}

async function getStakeEstInfo(wallet) {
    var result = await controllerstake.methods.getRewardEstimator(wallet).call();
    real["second"] = result[0];
    real["minute"] = result[1];
    real["hour"] = result[2];
    real["day"] = result[3];
    real["week"] = result[4];
    real["month"] = result[5];
    view["second"] = convert((result[0] / (10**18)));
    view["minute"] = convert((result[1] / (10**18)));
    view["hour"] = convert((result[2] / (10**18)));
    view["day"] = convert((result[3] / (10**18)));
    view["week"] = convert((result[4] / (10**18)));
    view["month"] = convert((result[5] / (10**18)));
    $("#sec").html(view["second"] + " " + stakedsymbol);
    $("#min").html(view["minute"] + " " + stakedsymbol);
    $("#hour").html(view["hour"] + " " + stakedsymbol);
    $("#day").html(view["day"] + " " + stakedsymbol);
    $("#week").html(view["week"] + " " + stakedsymbol);
    $("#month").html(view["month"] + " " + stakedsymbol);
}

async function approveYFDOT(amount) {
    try{
        var tx;
        var to18 = amount * (10**18);
        var newbn = new BigNumber(to18);
        if(parseFloat(view["balanceyfdot"]) >= amount){
            tx = await controllertokenmm.methods.approve(stakesc,newbn).send({from: account}, function(error, resi){
                var truncatetx = resi.substr(0, 10) + "....." + resi.substr(56, 66);
                if(resi != null){
                    toastInfo(5000, 'Pending TX :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+resi+'">'+truncatetx+'</a>');
                }
            }).on('receipt', (receipt) => {
                var truncatetx = receipt.transactionHash.substr(0, 10) + "....." + receipt.transactionHash.substr(56, 66);
                if(receipt.status){
                    toastSuccess(5000, 'TX Successfully :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+receipt.transactionHash+'">'+truncatetx+'</a>');
                }else{
                    toastError(5000, 'TX Failed :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+receipt.transactionHash+'">'+truncatetx+'</a>');
                }
            });
        }else{
            toastWarning(2000, 'Warning', 'Not enought balance');
        }
        setTimeout(function(){
            loader();
        },5000);
    }catch (error){
        toastError(3000, 'Error', error.message);
    }
}

async function stakeYFDOT(amount, wantperiod) {
    try{
        var tx;
        inittx = true;
        var to18 = amount * (10**18);
        var newbn = new BigNumber(to18);
        if(wantperiod == null){
            toastWarning(2000, 'Warning', 'Please choose stake period you want');
        }else{
            if(parseFloat(view["approvedyfdot"]) >= amount){
                tx = await controllerstakemm.methods.stakeNow(yfdottoken,newbn,wantperiod).send({from: account}, function(error, resi){
                    var truncatetx = resi.substr(0, 10) + "....." + resi.substr(56, 66);
                    if(resi != null){
                        toastInfo(5000, 'Pending TX :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+resi+'">'+truncatetx+'</a>');
                    }
                }).on('receipt', (receipt) => {
                    var truncatetx = receipt.transactionHash.substr(0, 10) + "....." + receipt.transactionHash.substr(56, 66);
                    if(receipt.status){
                        toastSuccess(5000, 'TX Successfully :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+receipt.transactionHash+'">'+truncatetx+'</a>');
                    }else{
                        toastError(5000, 'TX Failed :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+receipt.transactionHash+'">'+truncatetx+'</a>');
                    }
                });
            }else{
                toastWarning(2000, 'Warning', 'Not enought Approved balance, your allowance is : '+view["approvedyfdot"]+" YLFi");
            }
        }
        setTimeout(function(){
            loader();
        },5000);
    }catch (error){
        toastError(3000, 'Error', error.message);
    }
}

async function claimRewardStake() {
    try{
        var tx;
        inittx = true;
        var datatiame;
        var today = new Date();
        if(claimdate == 0){
            datatiame = moment(0).format('MM/DD/YYYY HH:mm:ss');
        }else{
            var newunixtime = claimdate * 1000;
            datatiame = moment(newunixtime).format('MM/DD/YYYY HH:mm:ss');
        }
        if(parseFloat(view["claimablereward"]) > 0 && today > claimdate){
            tx = await controllerstakemm.methods.claimReward().send({from: account}, function(error, resi){
                var truncatetx = resi.substr(0, 10) + "....." + resi.substr(56, 66);
                if(resi != null){
                    toastInfo(5000, 'Pending TX :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+resi+'">'+truncatetx+'</a>');
                }
            }).on('receipt', (receipt) => {
                var truncatetx = receipt.transactionHash.substr(0, 10) + "....." + receipt.transactionHash.substr(56, 66);
                if(receipt.status){
                    toastSuccess(5000, 'TX Successfully :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+receipt.transactionHash+'">'+truncatetx+'</a>');
                }else{
                    toastError(5000, 'TX Failed :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+receipt.transactionHash+'">'+truncatetx+'</a>');
                }
            });
        }else{
            toastWarning(2000, 'Warning', 'Not enought claimable balance or you still in cooldown time, you might claim at : '+datatiame);
        }
        setTimeout(function(){
            loader();
        },5000);
    }catch (error){
        toastError(3000, 'Error', error.message);
    }
}

async function unstakeYFDOT() {
    try{
        var tx;
        inittx = true;
        tx = await controllerstakemm.methods.unstakeNow().send({from: account}, function(error, resi){
            var truncatetx = resi.substr(0, 10) + "....." + resi.substr(56, 66);
            if(resi != null){
                toastInfo(5000, 'Pending TX :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+resi+'">'+truncatetx+'</a>');
            }
        }).on('receipt', (receipt) => {
            var truncatetx = receipt.transactionHash.substr(0, 10) + "....." + receipt.transactionHash.substr(56, 66);
            if(receipt.status){
                toastSuccess(5000, 'TX Successfully :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+receipt.transactionHash+'">'+truncatetx+'</a>');
            }else{
                toastError(5000, 'TX Failed :', '<a style="color:#000" target="_blank" href="'+etherscanlinktx+receipt.transactionHash+'">'+truncatetx+'</a>');
            }
        });
        setTimeout(function(){
            loader();
        },5000);
    }catch (error){
        toastError(3000, 'Error', error.message);
    }
}

if(window.ethereum){
    window.ethereum.on('accountsChanged', function () {
        connectDapps();
    });
    
    window.ethereum.on('chainChanged', () => {
      window.location.reload();
    });
}

// ui

$("#unlockwalletnow").click(async function(){
    sessionStorage.setItem("connect", true);
    await connectDapps();
});

if(sessionStorage.getItem("connect")){
    setTimeout(function() {
        connectDapps();
    }, 2000);
}

$("#amountstakes").on('change', function(){
    $("#rangerbal").val(0);
});

$("#claimnowstake").click(function(){
    claimRewardStake();
});

$("#unstakebtn").click(function(){
    unstakeYFDOT();
});

$("#approvebtn").click(function(){
    var amtstake = $("#amountstakes").val();
    approveYFDOT(amtstake);
});

$("#stakebtn").click(function(){
    var amtstake1 = $("#amountstakes").val();
    stakeYFDOT(amtstake1, wantperiod);
});

function setPeriod(val){
    wantperiod = val;
    $("#rppview").html('<span class="lbluetext">RPP</span> '+periodArray[val]['rppPercent']+'%');
    $("#perioddropdown").html(periodArray[val]['periodDay']+" Days");
}

function rangerbalance(value){
    var valuetobox = (view["balanceyfdot"] * value) / 100;
    $("#amountstakes").val(valuetobox.toFixed(3));
}

function validateUnfocus(words) {
    if(words.charAt(words.length - 1) == '.'){
        words = words + 0;
    }
    
    if(words.length > 6 && words < 0.00005){
        $("#amountstakes").val("");
        toastWarning(2000, 'Warning', 'Minimum stake is 0.00005 YLFi');
    }else{
        $("#amountstakes").val(words);
    }
}

function validate(valuea) {
    var rgx = /^[0-9]*\.?[0-9]*$/;
    var data = valuea;
    var res = data.replace(/[^.\d]/g, '').replace(/^(\d*\.?)|(\d*)\.?/g, "$1$2");
    if(res[0] == '.'){
        res = 0 + res;
    }
    $("#amountstakes").val(res);
}

function toastInfo(timeoutInMili, textTitle, textToast){
    iziToast.info({
        close: false,
        pauseOnHover: false,
        timeout: timeoutInMili,
        position: 'topRight',
        title: textTitle,
        message: textToast,
    });
}

function toastWarning(timeoutInMili, textTitle, textToast){
    iziToast.warning({
        close: false,
        pauseOnHover: false,
        timeout: timeoutInMili,
        position: 'topRight',
        title: textTitle,
        message: textToast,
    });
}

function toastError(timeoutInMili, textTitle, textToast){
    iziToast.error({
        close: false,
        pauseOnHover: false,
        timeout: timeoutInMili,
        position: 'topRight',
        title: textTitle,
        message: textToast,
    });
}

function toastSuccess(timeoutInMili, textTitle, textToast){
    iziToast.success({
        close: false,
        pauseOnHover: false,
        timeout: timeoutInMili,
        position: 'topRight',
        title: textTitle,
        message: textToast,
    });
}

async function changetextvalidator(vale){
    var counter = 1;
    for(var z = 1; z <= periodArray.length; z++){
        $("#period"+counter+"est").html('<div class="spinner-border spinner-border-sm" role="status"><span class="sr-only">Loading...</span></div>');
        var data = await getRewardCalculator(yfdottoken, vale, counter);
        data = data / (10**18);
        $("#period"+counter+"est").html(data+" YLFi");
        counter++;
    }
}

function convert(n){
    var sign = +n < 0 ? "-" : "",toStr = n.toString();
    if (!/e/i.test(toStr)) {
        return n;
    }
    var [lead,decimal,pow] = n.toString().replace(/^-/,"").replace(/^([0-9]+)(e.*)/,"$1.$2").split(/e|\./);
    return +pow < 0 ? sign + "0." + "0".repeat(Math.max(Math.abs(pow)-1 || 0, 0)) + lead + decimal : sign + lead + (+pow >= decimal.length ? (decimal + "0".repeat(Math.max(+pow-decimal.length || 0, 0))) : (decimal.slice(0,+pow)+"."+decimal.slice(+pow)));
}


setInterval(function(){
    loader();
}, 20000);
