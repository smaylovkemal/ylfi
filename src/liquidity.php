<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Liquidity - YFDOT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=yes">
    <link rel="icon" href="assets/img/webicon.ico" >
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/Footer-Basic.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/styles.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
</head>

<body>
    <?php include("header.php"); ?>
    <div class="intoduce">
        <div class="container">
            <h2 class="text-center text-white"><b>Liquidity <span class="badge badge-warning">Soon</span></b></h2>
        </div>
    </div>
    <div class="container">
        <div class="card mintop mb-2">
            <div class="card-body cardinfo">
                <div class="row">
                    <div class="col-sm-12 col-md-6 d-none d-md-block">
                        <img alt="yfdot basicinfo bg" class="tokeninfoimg" src="assets/img/tokeninfo.webp"/>
                    </div>
                    <div id="tkinfotext" class="col-sm-12 col-md-6">
                        <table id="tokenomictbl" class="table table-borderless">
                            <tbody>
                                <tr>
                                    <td colspan="2"><h4 class="mb-4 mt-4 text-white "><b class="modleimg">Coming soon, stay tuned with us!</b></h4></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include("footer.php"); ?>
    <?php include("seo/schema.php"); ?>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js"></script>
    <script src="assets/js/stakeinterface.js?data=<?php echo rand(1,99999999999999); ?>"></script>
</body>

</html>
