<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Farm - YFDOT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=yes">
    <link rel="icon" href="assets/img/webicon.ico" >
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/Footer-Basic.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/styles.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
</head>

<body>
    <?php include("header.php"); ?>
    <div class="intoduce">
        <div class="container">
            <h2 class="text-center text-white"><b>FARM <span class="badge badge-warning">MVP</span></b></h2>
            <p class="text-center greytext">Will be launched soon</p>
        </div>
    </div>
    <div class="container">
        <div class="card mintop">
            <div class="card-body stakess">
                <div class="row mb-4">
                    <div class="dropdown col-6 col-md-3 changepaddingcol">
                        <button class="btn btn-secondary btn-block dropdown-toggle togglemn" type="button" id="stakedropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img alt="yfdot selected currency" width="15%" height="auto" src="assets/img/yfdot-yfdot.png"/> YFDOT-YFDOT
                        </button>
                        <div class="dropdown-menu" aria-labelledby="stakedropdown">
                            <a class="dropdown-item" href="#"><img alt="yfdot-yfdot" width="15%" height="auto" src="assets/img/yfdot-yfdot.png"/> YFDOT-YFDOT</a>
                        </div>
                    </div>
                    <div class="dropdown col-6 col-md-3 changepaddingcol">
                        <button class="btn btn-secondary btn-block dropdown-toggle togglemn" type="button" id="perioddropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Select Period
                        </button>
                        <div class="dropdown-menu" aria-labelledby="perioddropdown">
                            <a class="dropdown-item" href="#">30 Days</a>
                            <a class="dropdown-item" href="#">60 Days</a>
                            <a class="dropdown-item" href="#">90 Days</a>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 changepaddingcol">
                        <p class="inlineinfo text-center">
                            <span class="greytext">APY</span> 5%
                        </p>
                    </div>
                    <div class="col-6 col-md-3 changepaddingcol">
                        <p class="inlineinfo text-center">
                            <span class="greytext">Locked</span> $100,000
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 linesrightver text-center">
                        <p class="greytext text-right">
                            Available Balance : 0 YFDOT
                        </p>
                        <input id="amountstakes" class="form-control form-control-lg inlineinfo mb-2 cardreg" type="text" placeholder="Amount to Stake">
                        <input type="range" class="custom-range mb-4" value="0" min="0" max="100" step="25" id="customRange2">
                        <button type="button" class="btn btn-light btn-lg stakebtn mb-5">Farm</button>
                    </div>
                    <div class="col-12 col-md-6 linesleftver">
                        <h4 class="text-white text-center mb-4">Reward Estimation</h4>
                        <div class="row">
                            <div class="col-6 changepaddingcol">
                                <p class="inlineinfo1 text-left">
                                    1 Period : 30 Days
                                </p>
                            </div>
                            <div class="col-6 changepaddingcol">
                                <p class="inlineinfo2 text-center">
                                    0 USDT
                                </p>
                            </div>
                            <div class="col-6 changepaddingcol">
                                <p class="inlineinfo1 text-left">
                                    2 Periods : 60 Days
                                </p>
                            </div>
                            <div class="col-6 changepaddingcol">
                                <p class="inlineinfo2 text-center">
                                    0 USDT
                                </p>
                            </div>
                            <div class="col-6 changepaddingcol">
                                <p class="inlineinfo1 text-left">
                                    3 Periods : 90 Days
                                </p>
                            </div>
                            <div class="col-6 changepaddingcol">
                                <p class="inlineinfo2 text-center">
                                    0 USDT
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-md-4 mb-3">
                <p class="text-center cardreg" style="color: #aab0b5">Staked Balance</p>
                <div class="stakecard2 card">
                    <div class="card-body">
                        <h3 id="initbalance" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <p class="text-center cardreg" style="color: #aab0b5">Total Balance</p>
                <div class="stakecard2 card">
                    <div class="card-body">
                        <h3 id="freebalance" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <p class="text-center cardreg" style="color: #aab0b5">Reward Obtained</p>
                <div class="stakecard2 card">
                    <div class="card-body">
                        <h3 id="rewardstake" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <p class="text-center cardreg" style="color: #aab0b5">Per Second</p>
                <div class="stakecard3 card">
                    <div class="card-body">
                        <h3 id="sec" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
            	<p class="text-center cardreg" style="color: #aab0b5">Per Minute</p>
                <div class="stakecard3 card">
                    <div class="card-body">
                        <h3 id="min" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <p class="text-center cardreg" style="color: #aab0b5">Per Hour</p>
                <div class="stakecard3 card">
                    <div class="card-body">
                        <h3 id="hour" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <p class="text-center cardreg" style="color: #aab0b5">Per Day</p>
                <div class="stakecard3 card">
                    <div class="card-body">
                        <h3 id="day" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <p class="text-center cardreg" style="color: #aab0b5">Per Week</p>
                <div class="stakecard3 card">
                    <div class="card-body">
                        <h3 id="week" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <p class="text-center cardreg" style="color: #aab0b5">Per Month</p>
                <div class="stakecard3 card">
                    <div class="card-body">
                        <h3 id="month" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-md-4 mb-3"></div>
            <div class="col-md-4 mb-3">
                <p class="text-center cardreg" style="color: #aab0b5">Claimable</p>
                <div class="stakecard card pointer">
                    <div class="card-body">
                        <h3 id="claimable" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-4 mb-3"></div>
            <div class="col-md-4 mb-3">
                <p class="text-center cardreg" style="color: #aab0b5">Total Claim</p>
                <div class="stakecard1 card">
                    <div class="card-body">
                        <h3 id="totalreward" class="text-center card-title cardjdl" style="color: #fbfbfb; margin: 0 !important;">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include("footer.php"); ?>
    <?php include("seo/schema.php"); ?>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js"></script>
    <script src="assets/js/stakeinterface.js?data=<?php echo rand(1,99999999999999); ?>"></script>
</body>

</html>
