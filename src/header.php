<?php if ( ! defined('BASEPATH')) exit(header("location: /")); ?>
<nav class="navbar navbar-light navbar-expand-md navigation-clean-button" style="background-color: #263340;">
    <div class="container">
        <a class="navbar-brand" id="navbar-brand" href="https://yfdot.finance/"><img alt="yfdot logo" class="navbar-logo" src="assets/img/logo.png"/>YFDOT</a>
        <button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1">
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul id="menu" class="nav navbar-nav justify-content-center">
                <li class="nav-item" role="presentation"><a class="nav-link <?php if($pname == "home"){echo "active";} ?>" href="/">Home</a></li>
                <li class="nav-item dropdown"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#">Platform </a>
                    <div class="dropdown-menu navdown" role="menu">
                        <a class="dropdown-item <?php if($pname == "stake"){echo "active";} ?>" role="presentation" href="/stake">Stake</a>
                        <a class="dropdown-item <?php if($pname == "farm"){echo "active";} ?>" role="presentation" href="/farm">Farm</a>
                        <a class="dropdown-item <?php if($pname == "barter"){echo "active";} ?>" role="presentation" href="/barter">Barter</a>
                        <a class="dropdown-item <?php if($pname == "borrow"){echo "active";} ?>" role="presentation" href="/borrow">Borrow</a>
                        <a class="dropdown-item <?php if($pname == "mortgage"){echo "active";} ?>" role="presentation" href="/mortgage">Mortgage</a>
                    </div>
                </li>
                <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="modal" data-target="#latestupdate">Latest Update</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link <?php if($pname == "liquidity"){echo "active";} ?>" href="/liquidity">Liquidity</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="https://t.me/YFDOT_Official">Telegram</a></li>
            </ul>
            <span class="navbar-text actions ml-auto">
                <?php
                    if($unlock == true){
                ?>
                <button id="unlockwalletnow" class="navbar-brand btn btn-outline-light btnconnect" role="button">Unlock Wallet</button>
                <?php
                    }else{
                ?>
                <a class="btn btn-outline-light btnconnect" role="button" href="/stake">Stake Now</a>
                <?php
                    }
                ?>
            </span>
        </div>
    </div>
</nav>

<div class="modal fade" id="latestupdate" tabindex="-1" role="dialog" aria-labelledby="latestupdatetwitter" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h2 class="featurette-heading text-center text-white cardjdl mb-3" style="margin-left: 1em; padding: 1rem;">Latest Update</h2>
			</div>
			<div class="modal-body">
			    <a class="twitter-timeline" href="https://twitter.com/Yfdotfinance?ref_src=twsrc%5Etfw">Tweets by Yfdotfinance</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
			</div>
		</div>
	</div>
</div>
