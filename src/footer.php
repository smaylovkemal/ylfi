<?php if ( ! defined('BASEPATH')) exit(header("location: /")); ?>
<div class="footer-basic" style="background-color: #263340;">
    <footer>
        <div class="social">
            <a target="_blank" href="https://twitter.com/Yfdotfinance"><img  alt="yfdot social media twitter" class="social-logo" src="assets/img/twitter.png"/></i></a>
            <a target="_blank" href="https://discord.gg/9f7vKv4"><img  alt="yfdot social media discord" class="social-logo" src="assets/img/discord.png"/></i></a>
            <a target="_blank" href="https://github.com/YFDOT"><img  alt="yfdot social media github" class="social-logo" src="assets/img/github.png"/></i></a>
            <a target="_blank" href="https://medium.com/@YFdotFinance"><img  alt="yfdot social media medium" class="social-logo" src="assets/img/medium.png"/></i></a>
            <a target="_blank" href="https://t.me/YFDOT_Finance"><img  alt="yfdot social media telegram" class="social-logo" src="assets/img/telegram.png"/></i></a>
        </div>
        <p class="copyright">Copyright &#169; 2020 YFDOT. All Right Reserved.</p>
    </footer>
</div>